# Chinese translations for kdeconnect-kde package
# kdeconnect-kde 套件的正體中文翻譯.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Automatically generated, 2019.
# pan93412 <pan93412@gmail.com>, 2019.
# Chaoting Liu <brli@chakralinux.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-09-12 09:15+0800\n"
"Last-Translator: Chaoting Liu <brli@chakralinux.org>\n"
"Language-Team: Chinese <kde-i18n-doc@kde.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "pan93412, Chating Liu"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "pan93412@gmail.com"

#: main.cpp:30 main.cpp:32
#, kde-format
msgid "KDE Connect"
msgstr "KDE 連線"

#: main.cpp:34
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015, Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Maintainer"
msgstr "維護者"

#: main.cpp:51
#, kde-format
msgid "URL to share"
msgstr "要分享的網址"

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "取消配對"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "傳送 Ping"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:16
#, kde-format
msgid "Plugin Settings"
msgstr "擴充元件設定"

#: qml/DevicePage.qml:63
#, kde-format
msgid "Multimedia control"
msgstr "多媒體控制"

#: qml/DevicePage.qml:70
#, kde-format
msgid "Remote input"
msgstr "遠端輸入"

#: qml/DevicePage.qml:77 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "遠端簡報"

#: qml/DevicePage.qml:86 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "鎖定"

#: qml/DevicePage.qml:86
#, kde-format
msgid "Unlock"
msgstr "解鎖"

#: qml/DevicePage.qml:93
#, kde-format
msgid "Find Device"
msgstr "尋找裝置"

#: qml/DevicePage.qml:98 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "執行指令"

#: qml/DevicePage.qml:106
#, kde-format
msgid "Share File"
msgstr "分享檔案"

#: qml/DevicePage.qml:111 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "音量控制"

#: qml/DevicePage.qml:120
#, kde-format
msgid "This device is not paired"
msgstr "本裝置未配對"

#: qml/DevicePage.qml:124 qml/FindDevicesPage.qml:23
#, kde-format
msgid "Pair"
msgstr "配對"

#: qml/DevicePage.qml:136
#, kde-format
msgid "Pair requested"
msgstr "送出配對請求"

#: qml/DevicePage.qml:142
#, kde-format
msgid "Accept"
msgstr "接受"

#: qml/DevicePage.qml:148
#, kde-format
msgid "Reject"
msgstr "拒絕"

#: qml/DevicePage.qml:157
#, kde-format
msgid "This device is not reachable"
msgstr "無法連線裝置"

#: qml/DevicePage.qml:165
#, kde-format
msgid "Please choose a file"
msgstr "請選擇檔案"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "找不到裝置"

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "已記住"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "可用"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "已連線"

#: qml/main.qml:28
#, kde-format
msgid "Find devices..."
msgstr "尋找裝置…"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "遠端控制"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press the left and right mouse buttons at the same time to unlock"
msgstr "同時按下滑鼠左鍵與右鍵以解鎖"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "多媒體控制"

#: qml/mpris.qml:61
#, kde-format
msgid "No players available"
msgstr "無可用播放器"

#: qml/mpris.qml:109
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "啟用全螢幕"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "編輯指令"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "您能編輯連線裝置的指令"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "未定義指令"
